.reinstall_and_start_android_app: &reinstall_and_start_android_app
  - adb uninstall fi.altom.altunitytester || true
  - adb install sampleGame.apk
  - adb shell am start -n fi.altom.altunitytester/com.unity3d.player.UnityPlayerActivity
  - adb forward --remove-all
  - adb forward tcp:13000 tcp:13000

.initialize_venv: &initialize_venv
  - >
    if [ -d altunitytests ]; then
      echo "Activating virtual environment."
      source altunitytests/bin/activate
      echo "Virtual environment has been activated."
    else
      echo "Creating virtual environment."
      python3 -m venv altunitytests
      source altunitytests/bin/activate
      echo "Virtual environment has been created and activated."
    fi

.pip_install_dependencies: &pip_install_dependencies
  - pip install Appium-Python-Client
  - pip uninstall --yes altunityrunner || true
  - pip install -e "Bindings~/python"
  - pip install Deprecated
  - pip install pytest

workflow:
  rules:
    - if: '$CI_PIPELINE_SOURCE == "push" && $CI_COMMIT_REF_NAME != "master" && $CI_COMMIT_REF_NAME != "development"'
      when: never
    - when: always

stages:
  - build
  - test
  - docs
  - alphadocs
  - deploy

cache:
  key: ${CI_COMMIT_REF_SLUG}
  paths:
    - sampleGame.apk
    - sampleGame/
    - .cache/pip
    - altunitytests/

build-apk:
  stage: build
  rules:
    - if: '$CI_COMMIT_REF_NAME == "master" && $CI_COMMIT_MESSAGE =~ /^Documentation.*/'
      when: never
    - when: always
  tags:
    - android
  script:
    - whoami
    - adb uninstall fi.altom.altunitytester || true
    - ${UNITY_2019_2_HOME:-/Applications/Unity/Hub/Editor/2019.2.0f1/Unity.app/Contents/MacOS/Unity} -batchmode -stackTraceLogType None -projectPath $CI_PROJECT_DIR -executeMethod BuildAltUnityTester.AndroidBuildFromCommandLine -logFile buildAndroid.log -quit
    - ls
  cache:
    paths:
      - sampleGame.apk
  artifacts:
    when: always
    expire_in: 10 days
    paths:
      - buildAndroid.log
      - sampleGame.apk

# build-ipa:
#     stage: build
#     rules:
#       - if: '$CI_COMMIT_REF_NAME == "master" && $CI_COMMIT_MESSAGE =~ /^Documentation.*/'
#         when: never
#       - when: always
#     tags:
#       - iPhone
#     script:
#       - whoami
#       - ${UNITY_2019_2_HOME:-/Applications/Unity/Hub/Editor/2019.2.0f1/Unity.app/Contents/MacOS/Unity} -batchmode -projectPath $CI_PROJECT_DIR -executeMethod BuildAltUnityTester.IosBuildFromCommandLine -logFile buildIos.log -quit
#       - cd sampleGame
#       - xcodebuild build-for-testing -scheme Unity-iPhone -destination generic/platform=iOS -allowProvisioningUpdates
#       - ls
#     artifacts:
#       when: always
#       expire_in: 10 days
#       paths:
#         - sampleGame/
#         - buildIos.log

run-csharp-unity-android-tests:
  stage: test
  rules:
    - if: '$CI_COMMIT_REF_NAME == "master" && $CI_COMMIT_MESSAGE =~ /^Documentation.*/'
      when: never
    - when: always
  tags:
    - android
  script:
    - ls
    - *reinstall_and_start_android_app
    - ${UNITY_2019_2_HOME:-/Applications/Unity/Hub/Editor/2019.2.0f1/Unity.app/Contents/MacOS/Unity} -batchmode -stackTraceLogType None -projectPath $CI_PROJECT_DIR -executeMethod AltUnityTestRunner.RunTestFromCommandLine -logFile csharpAndroidTests.log -quit
  after_script:
    - adb forward --remove-all
    - cat csharpAndroidTests.log
  artifacts:
    when: always
    expire_in: 10 days
    paths:
      - csharpAndroidTests.log
      - AltUnityTesterLog.txt
  needs: ["build-apk"]

run-csharp-dotnet-android-tests:
  stage: test
  rules:
    - if: '$CI_COMMIT_REF_NAME == "master" && $CI_COMMIT_MESSAGE =~ /^Documentation.*/'
      when: never
    - when: always
  tags:
    - android
  script:
    - *reinstall_and_start_android_app
    - dotnet test Bindings~/dotnet/AltUnityDriver.Tests/AltUnityDriver.Tests.csproj
  after_script:
    - adb forward --remove-all
  artifacts:
    when: always
    expire_in: 10 days
    paths:
      - AltUnityTesterLog.txt
  needs: ["build-apk"]

run-python-android-tests:
  stage: test
  tags:
    - android
  script:
    - *reinstall_and_start_android_app
    - *initialize_venv
    - *pip_install_dependencies
    - python Bindings~/python/tests/python_bindings_tests.py
    - pytest Bindings~/python/tests/unit
  after_script:
    - adb forward --remove-all
  artifacts:
    when: always
    expire_in: 10 days
    paths:
      - AltUnityTesterLog.txt
  needs: ["build-apk"]

run-java-android-tests:
  stage: test
  rules:
    - if: '$CI_COMMIT_REF_NAME == "master" && $CI_COMMIT_MESSAGE =~ /^Documentation.*/'
      when: never
    - when: always
  tags:
    - android
  script:
    - *reinstall_and_start_android_app
    - cd "Bindings~/java"
    - mvn -Dtest=ro.altom.altunitytester.Tests* test
  after_script:
    - adb forward --remove-all
  artifacts:
    when: always
    expire_in: 10 days
    paths:
      - AltUnityTesterLog.txt
  needs: ["build-apk"]

run-python-appium-android-tests:
  stage: test
  rules:
    - if: '$CI_COMMIT_REF_NAME == "master" && $CI_COMMIT_MESSAGE =~ /^Documentation.*/'
      when: never
    - when: always
  tags:
    - android
  script:
    - *initialize_venv
    - *pip_install_dependencies
    - python "Bindings~/python/tests/python_appium_tests.py"
  after_script:
    - adb forward --remove-all
  artifacts:
    when: always
    expire_in: 10 days
    paths:
      - appium.log
      - AltUnityTesterLog.txt
  needs: ["build-apk"]

run-java-appium-android-tests:
  stage: test
  rules:
    - if: '$CI_COMMIT_REF_NAME == "master" && $CI_COMMIT_MESSAGE =~ /^Documentation.*/'
      when: never
    - when: always
  tags:
    - android
  script:
    - whoami
    - cd "Bindings~/java"
    - mvn clean compile assembly:single
    - mvn install:install-file -Dfile=./target/altunitytester-java-client-jar-with-dependencies.jar -DgroupId=ro.altom -DartifactId=altunitytester -Dversion=1.6.2 -Dpackaging=jar
    - cd ../java-appium-tests
    - mvn test
  after_script:
    - adb forward --remove-all
  artifacts:
    when: always
    expire_in: 10 days
    paths:
      - AltUnityTesterLog.txt
      - "./Bindings~/java/target/altunitytester-java-client-jar-with-dependencies.jar"
  needs: ["build-apk"]

# run-csharp-ios-tests:
#     stage: test
#     rules:
#         - if: '$CI_COMMIT_REF_NAME == "master" && $CI_COMMIT_MESSAGE =~ /^Documentation.*/'
#           when: never
#         - when: always
#     tags:
#         - iPhone
#     script:
#         - cd sampleGame
#         - iproxy 13000 13000 &
#         - xcodebuild test-without-building -destination 'platform=iOS,name=iPhone' -scheme Unity-iPhone -allowProvisioningUpdates &
#         - sleep 60
#         - ${UNITY_2019_2_HOME:-/Applications/Unity/Hub/Editor/2019.2.0f1/Unity.app/Contents/MacOS/Unity} -projectPath $CI_PROJECT_DIR -executeMethod AltUnityTestRunner.RunTestFromCommandLine -logFile csharpIosTests.log -quit
#         - killall iproxy || true
#         - killall xcodebuild || true
#     artifacts:
#       when: always
#       expire_in: 10 days
#       paths:
#         - sampleGame/
#         - csharpIosTests.log
#     needs: ["build-ipa"]

create-unity-package:
  stage: docs
  rules:
    - if: '$CI_COMMIT_REF_NAME == "master" && $CI_COMMIT_MESSAGE =~ /^Documentation.*/'
      when: never
    - if: '$CI_COMMIT_REF_NAME == "master"'
      when: always
    - when: never
  tags:
    - unity
    - mac
  script:
    - mkdir -p public/${CI_COMMIT_REF_NAME}/AltUnityPackage || true
    - ${UNITY_2019_2_HOME:-/Applications/Unity/Hub/Editor/2019.2.0f1/Unity.app/Contents/MacOS/Unity} -batchmode -projectPath $CI_PROJECT_DIR -executeMethod AltUnityTesterEditor.CreateAltUnityTesterPackage -logFile createAltUnityTesterPackage.log -quit
    - mv AltUnityTester.unitypackage public/${CI_COMMIT_REF_NAME}/AltUnityPackage/AltUnityTester.unitypackage
    - cd public/${CI_COMMIT_REF_NAME}/AltUnityPackage
    - echo '<a href="https://altom.gitlab.io/altunity/altunitytester/'${CI_COMMIT_REF_NAME}'/AltUnityPackage/AltUnityTester.unitypackage">AltUnityTester.unitypackage</a>' > index.html
    - ls
  artifacts:
    when: always
    paths:
      - public

create-jar-archive:
  stage: docs
  rules:
    - if: '$CI_COMMIT_REF_NAME == "master" && $CI_COMMIT_MESSAGE =~ /^Documentation.*/'
      when: never
    - if: '$CI_COMMIT_REF_NAME == "master"'
      when: always
    - when: never
  tags:
    - unity
    - mac
  script:
    - mkdir -p public/${CI_COMMIT_REF_NAME}/AltUnityJAR || true
    - cd "Bindings~/java"
    - mvn clean compile assembly:single
    - mv target/altunitytester-java-client-jar-with-dependencies.jar ../../public/${CI_COMMIT_REF_NAME}/AltUnityJAR/altunitytester-java-client-jar.jar
    - cd ../../public/${CI_COMMIT_REF_NAME}/
    - echo '<a href="https://altom.gitlab.io/altunity/altunitytester/'${CI_COMMIT_REF_NAME}'/AltUnityJAR/altunitytester-java-client.jar">altunitytester-java-client-jar-with-dependencies.jar</a>' > index.html
    - ls
    - cat index.html
  artifacts:
    when: always
    paths:
      - public

deploy-altunitytester-python:
  stage: deploy
  rules:
    - if: '$CI_COMMIT_REF_NAME == "master" && $CI_COMMIT_MESSAGE =~ /^Documentation.*/'
      when: never
    - if: '$CI_COMMIT_REF_NAME == "master"'
      when: manual
    - when: never
  tags:
    - unity
    - mac
    - android
  script:
    - cd "Bindings~/python"
    - python setup.py sdist upload -r pypi

deploy-altunitydriver-dotnet:
  stage: deploy
  rules:
    - if: '$CI_COMMIT_REF_NAME == "master" && $CI_COMMIT_MESSAGE =~ /^Documentation.*/'
      when: never
    - if: '$CI_COMMIT_REF_NAME == "master"'
      when: manual
    - when: never
  tags:
    - unity
    - mac
    - android
  script:
    - cd "Bindings~/dotnet"
    - dotnet pack AltUnityDriver/AltUnityDriver.csproj -c release
    - dotnet nuget push AltUnityDriver/bin/release/AltUnityDriver.1.6.2.nupkg --api-key $NUGET_DEPLOY_KEY --source https://api.nuget.org/v3/index.json

alphadocs:
  stage: docs
  rules:
    - if: '$CI_COMMIT_REF_NAME == "master" && $CI_COMMIT_MESSAGE =~ /^Documentation.*/'
      when: never
    - if: '$CI_COMMIT_REF_NAME == "master"'
      when: always
    - when: never
  trigger: altom/altunity/altunitytester-alpha

pages:
  stage: deploy
  rules:
    - if: '$CI_COMMIT_REF_NAME == "master" && $CI_COMMIT_MESSAGE =~ /^Documentation.*/'
      when: never
    - if: '$CI_COMMIT_REF_NAME == "master"'
      when: manual
    - when: never
  tags:
    - unity
    - mac
  artifacts:
    paths:
      - public
  script:
    - cd docs
    - *initialize_venv
    - python3 -m pip install -r requirements.txt
    - make html
    - cp -r _build/html/. ../public/

maven-snapshot-repo:
  stage: deploy
  rules:
    - if: '$CI_COMMIT_REF_NAME == "master" && $CI_COMMIT_MESSAGE =~ /^Documentation.*/'
      when: never
    - if: '$CI_COMMIT_REF_NAME == "master"'
      when: manual
    - when: never
  script:
    - cd "Bindings~/java"
    - mvn deploy -DskipTests
  tags:
    - unity
    - mac

maven-repo:
  stage: deploy
  rules:
    - if: '$CI_COMMIT_REF_NAME == "master" && $CI_COMMIT_MESSAGE =~ /^Documentation.*/'
      when: never
    - if: '$CI_COMMIT_REF_NAME == "master"'
      when: manual
    - when: never
  script:
    - cd "Bindings~/java"
    - mvn deploy -f pom-release.xml -DskipTests
  tags:
    - unity
    - mac
