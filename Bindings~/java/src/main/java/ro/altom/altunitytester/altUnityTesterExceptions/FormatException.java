package ro.altom.altunitytester.altUnityTesterExceptions;

public class FormatException extends AltUnityException {
    /**
     *
     */
    private static final long serialVersionUID = -4152994574165513829L;

    public FormatException() {
    }

    public FormatException(String message) {
        super(message);
    }
}
