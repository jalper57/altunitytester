package ro.altom.altunitytester.altUnityTesterExceptions;

public class WaitTimeOutException extends AltUnityException {
    /**
     *
     */
    private static final long serialVersionUID = 4896864591826296908L;

    public WaitTimeOutException() {
    }

    public WaitTimeOutException(String message) {
        super(message);
    }
}
