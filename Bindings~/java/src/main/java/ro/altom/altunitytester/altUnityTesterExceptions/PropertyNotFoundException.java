package ro.altom.altunitytester.altUnityTesterExceptions;

public class PropertyNotFoundException extends AltUnityException {
    /**
     *
     */
    private static final long serialVersionUID = -1119682435844523950L;

    public PropertyNotFoundException() {
    }

    public PropertyNotFoundException(String message) {
        super(message);
    }
}
